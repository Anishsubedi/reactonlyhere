import React, { Component } from "react";
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  Breadcrumb,
  BreadcrumbItem
} from "reactstrap";
import { Link } from "react-router-dom";
import Menu from "./MenuComponent";

function RenderDish({ item }) {
  if (item.length > 0)
    return (
      <div className="container">
        <div className="row">
          <Breadcrumb>
            <BreadcrumbItem>
              <Link to="/menu">Menu</Link>
            </BreadcrumbItem>
            <BreadcrumbItem active>{item.name} </BreadcrumbItem>
          </Breadcrumb>
          <div className="col-12" />
          <h3>{item.name}</h3>
          <hr />
        </div>
        <div className="row">{Menu}</div>

        <div className="row">
          <div className="col-12 col-md-5 m-1">
            <Card>
              <CardImg top src={item[0].image} alt={item[0].name} />
              <CardBody>
                <CardTitle>{item[0].name}</CardTitle>
                <CardText>{item[0].description}</CardText>
                <CardText>{item[0].comment}</CardText>
              </CardBody>
            </Card>
          </div>
          <div className="col">
            <RenderComments coms={item[0]} />
          </div>
        </div>
      </div>
    );
  else return <div> No items here</div>;
}

function RenderComments(coms) {
  const newcoms = coms.coms;

  const items1 = newcoms.comments.map(dish => {
    console.log(dish);
    return (
      <div>
        <div key={dish.id}>
          <ul>
            <li> {dish.comment}</li>
            <li>
              --{dish.author} , {dish.date}
            </li>
          </ul>
        </div>
      </div>
    );
  });

  if (newcoms.comments != null) {
    return (
      <div>
        <h4>Comments</h4>
        {items1}
      </div>
    );
  } else {
    return <div />;
  }
}
const DishDetail = props => {
  console.log(props.dish);
  return (
    <div>
      <div>
        {" "}
        <RenderDish item={props.dish} />
      </div>
    </div>
  );
};

export default DishDetail;
